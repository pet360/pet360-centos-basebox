require 'spec_helper'

describe package('chefdk') do
  it { should be_installed }
end

describe package('chef') do
  it { should be_installed.with_version('12.5.1') }
end

describe yumrepo('pbiso') do
  it { should exist }
  it { should be_enabled }
end

describe package('pbis-open') do
  it { should be_installed }
end

describe command('which domainjoin-cli') do
  its (:stdout) { should match '/usr/bin/domainjoin-cli' }
end

describe file('/usr/bin/domainjoin-cli') do
  it { should be_file }
end

describe file('/etc/hosts') do
  HOSTS = [
    '127.0.0.1 localhost.localdomain localhost',
    '127.0.0.1 localhost4.localdomain4 localhost4',
    '::1 localhost.localdomain localhost',
    '::1 localhost6.localdomain6 localhost6'
  ]
  HOSTS.each do |host|
    its(:content) { should match(host) }
  end
end

describe file('/etc/cloud/cloud.cfg') do
  it { should be_file }
  its(:content) do
    should_not match(/^manage_etc_hosts:\strue$/)
  end
  its(:content) do
    should_not match(/^preserve_hostname:\strue$/)
  end
  its(:content) do
    should match(/datasource_list: \[ Ec2, None \]/)
  end
  its(:content) do
    should match(
      %r! - cloud-init-per instance my_etc_hosts sh -xc "echo -e '\\n'\$\(curl -s http://169.254.169.254/latest/meta-data/local-ipv4\) \$\(hostname\) \$\(hostname -s\)'\\n' >>/etc/hosts"!
    )
  end
end
