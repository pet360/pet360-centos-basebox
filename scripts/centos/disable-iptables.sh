#!/bin/bash -eux

# Stop iptables
/sbin/service iptables stop

# Remove iptables from startup
/sbin/chkconfig iptables off
