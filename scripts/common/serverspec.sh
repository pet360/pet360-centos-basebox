#!/bin/bash

eval "$(chef shell-init bash)"

cd /tmp/serverspec
bundle install --path vendor
bundle exec rake spec
