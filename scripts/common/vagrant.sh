#!/bin/bash

if [[ "$PACKER_BUILDER_TYPE" == virtualbox* ]]; then
    mkdir /home/vagrant/.ssh
    wget --no-check-certificate \
	'https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub' \
	-O /home/vagrant/.ssh/authorized_keys
    chown -R vagrant /home/vagrant/.ssh
    chmod -R go-rwsx /home/vagrant/.ssh
else
    echo 'Vagrant not needed for rackspace.'
fi
