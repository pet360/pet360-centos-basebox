#Overview
This repo contains the packer scripts to generate baseboxes for vagrant, aws and rackspace. The repo currently supports building CentOS 6 for vagrant, AWS and Rackspace. There is a packer script for CentOS 7 but that has not been tested or setup in all three environments.

Note: mirrors.kernel.org/centos has changed the way the store iso files; they now only store the latest version.  The packer template will need to be updated every time the version changes; otherwise, the sha1 checksum validation will fail.

The vagrant portion was based off the bento project on [github](https://github.com/chef/bento).

#Requirements
Be sure to have the following setup before attempting to build any boxes:

* [Packer](https://www.packer.io) - [Version 0.10+](https://www.packer.io/downloads.html)
* [Vagrant](https://www.vagrantup.com)
* (Required for AWS)AWS Account access
* [Atlas](https://atlas.hashicorp.com) access - For updating the Pet360 Vagrant baseboxes
  * Setup the `ATLAS_TOKEN` environment variable with a valid token in order to access the basebox.

#Usage
The following sections outline how to test and build the various baseboxes, AMIs and Snapshots.

##Releasing a new CentOS 6 Amazon AMI
The Packer script is set to deregister images of the same name so it builds an AMI named `PET360-CENTOS6-BASE-PACKER-TEST` to avoid destroying a working AMI. This also allows for manually testing before rebuilding over the working AMI.

The workflow is as follows:

1. Write serverspec tests in the `serverspec/spec/localhost/default_spec.rb` file to validate the changes you want to make to the base image.

2. Build the image using the default AMI name. If the tests fail, the image will not be saved:

   ```
   packer build -only=amazon-ebs centos-6-x86_64.json
   ```
        
3. (Optional) Perform any manual tests against an instance spun up with the `PET360-CENTOS6-BASE-PACKER-TEST` AMI.
4. Replace the current `PET360-CENTOS6-BASE-PACKER` AMI:

   ```
   packer build -only=amazon-ebs -var 'ami_name=PET360-CENTOS6-BASE-PACKER' centos-6-x86_64.json
   ```

##Releasing a new CentOS 6 Vagrant basebox

1. Build the Virtualbox basebox with packer:

	```
	packer build -only=virtualbox-iso centos-6-x86_64.json
	```
		
2. Currently the atlas upload is broken so the basebox will have to be uploaded manually to Atlas. In Atlas, [create a new version](https://atlas.hashicorp.com/pet360/boxes/centos-65-chef/versions/new) for the **pet360/centos-65-chef** basebox and upload the box in the `builds` directory.

###To Test
Be sure your `ATLAS_TOKEN` environment variable is set to a valid token before proceeding.

1. Create a Vagrantfile using the `pet360/centos-65-chef` basebox:
		
		vagrant init pet360/centos-65-chef
		
2. Create the vm

		vagrant up

##Releasing a new Docker base image
This will create a CentOS base image for Docker and push it to Docker Hub. This base image is used by petmd-cookbook/packer/petmd.json
	```
	packer build -only=docker centos-6-x86_64.json
	```

##TODO
* Register a Docker Hub Enterprise account and update the Packer credentials to use it